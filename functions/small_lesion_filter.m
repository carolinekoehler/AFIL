function LM=small_lesion_filter(n,LM,LBL_lesion1,NUM_lesion1,Volume1)

% n= size of lesion (in Voxel) which should be excluded 
% LM= raw Lesion matrix
% LBL_lesion1 = labeled Lesion matrix 
% NUM_lesion1 = number of leions point in time 1
% Volume1= Volume of lesions time in point 1

% x_1= matrix with dedicated lesion label

mini_lesion_ex=Volume1>n;

    for n=1:NUM_lesion1;
        if mini_lesion_ex(n)==1;
           %fpintf('L�sion akzeptiert');
        else x_1=(LBL_lesion1==n);
             LM(find(x_1))=0;               % in raw LM werden L�sionen <n mit 0 ersetzt und eliminiert
        end                                 % LM wird �berschrieben
    end