%function Volumen=Volume_calculator_and_LVTM_relabeled(ldata,ldata(1).LLTM,timepoints)
function ldata=Volume_calculator_and_LVTM_relabeled(ldata,ldata_Lprep)
% function Volumen=volumenberechnung(lesion1,NUM_lesion1)
%
% ldata(i).labels_relabeled.img = gelabelte L�sionsmatrix 
% NUM_lesion1 = Anzahl der L�sionen
% global_Labelarray= corrected LLTM for separating and confluencing lesions

% r= Label(L�sion1...L�sion n)aus gelabelter Matrix eine Zeitpunktes durchl�uft Schleife
% x_1= alle Matrixelemente mit jeweiligem Label der Schleife werden aufsummiert 
% ausgegeben werden Label und zugeh�riges Volumen

timepoints=length(ldata_Lprep);

ldata(1).LVTM = zeros(size(ldata(1).LLTM));  % initialisiere Vektor Volumen mit Nullen um Ergebnisse aus Schleife zu speichern, 1 eine Spalte  

for r=1:size(ldata(1).LLTM,1);
    ldata(1).LVTM(r,1)=r;
end

for c=2:timepoints+1;
    for r=1:size(ldata(1).LLTM,1);           % Schleifenbedingung: 1 bis Anzahl L�sionen 
       if isnan(ldata(1).LLTM{r,c});
          ldata(1).LVTM(r,c)=nan;
       else
          x_1=ldata_Lprep(c-1).labels_relabeled.img==r;     % x_1 = findet alle Elemente eines Labels in der gelabelten L�sionsmatrix
          ldata(1).LVTM(r,c)=sum(x_1(:)); % Volumen eines Labels (r) ist die Summe der Elemente eines Labels
       end
    end   
end
