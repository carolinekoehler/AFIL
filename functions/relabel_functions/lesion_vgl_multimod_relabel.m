function lschnittmenge=lesion_vgl_multimod_relabel(LBL_lesion1,NUM_lesion1_vektor,LBL_lesion2,...
                                  NUM_lesion2_vektor)
%

NUM_lesion1=length(NUM_lesion1_vektor);
NUM_lesion2=length(NUM_lesion2_vektor);

% lschnittmenge=cell(NUM_lesion2,1);
lschnittmenge=cell(max(cell2mat(NUM_lesion2_vektor)),1); 
% Berechnungsstatus
h=waitbar(0,sprintf('Comparing %ix%i lesions...',NUM_lesion1,NUM_lesion2),...
          'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');              
setappdata(h,'canceling',0);
for i=1:NUM_lesion2              % �u�ere Schleifenwiederholung 
                                 % 1 bis Anzahl der L�sionen in Zeitpunkt 2 
  if getappdata(h,'canceling')
    lschnittmenge={};
    break
  end    
  tmp=(LBL_lesion2.img==NUM_lesion2_vektor{i});                     
  for j=1:NUM_lesion1                       
    % Check for Cancel button press
    schnittmenge=tmp & (LBL_lesion1.img==NUM_lesion1_vektor{j});    
    if(any(schnittmenge(:)))                
      lschnittmenge{NUM_lesion2_vektor{i}}(end+1)=NUM_lesion1_vektor{j};           
    end                                     
  end
  waitbar(i/NUM_lesion2,h)
end
delete(h)
  