# AFIL

Exploring Individual Multiple Sclerosis Lesion Volume Change Over Time
Development of an Algorithm for the Analyses of Longitudinal Quantitative MRI Measures 

Magnetic resonance imaging (MRI) is used to follow-up multiple sclerosis (MS) and evaluate disease progression and therapy response via lesion quantification. However, there is a lack of automated post-processing techniques to quantify individual MS lesion change.

An Automatic Follow-up of Individual Lesions (AFIL) algorithm was developed to process time series of pre-segmented binary lesion masks. The resulting consistently labelled lesion masks allowed for the evaluation of individual lesion volumes. 

The algorithm considered stable, growing, or shrinking lesion volumes, as well as resolving, new, reappearing, confluent and separating lesion behavior. 

### USAGE:
mandatory use already registered binary lesion masks of consecutive time points
1. insert description --> `FLAIR_Lprep(1).desc='FLAIR_0m';` of the amount of your time points 
2. insert lesion masks via `desc2fname=@(x)['.\data\P007-' x '-Lesion_T1_BL_EDITED2.nii'] `
3. insert a white matter mask

